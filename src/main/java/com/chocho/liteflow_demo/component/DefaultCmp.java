package com.chocho.liteflow_demo.component;

import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.core.NodeComponent;
import com.yomahub.liteflow.log.LFLog;
import com.yomahub.liteflow.log.LFLoggerManager;

@LiteflowComponent(id = "fallback", name = "降级处理组件")
@FallbackCmp
public class DefaultCmp extends NodeComponent {
    private final LFLog logger = LFLoggerManager.getLogger(FlowExecutor.class);
    @Override
    public void process() {
        logger.info("\n组件缺失，系统异常，请稍后再试!\n");
    }
}
