package com.chocho.liteflow_demo.component;

import com.yomahub.liteflow.core.NodeComponent;

/**
 * 用于验证chain中的节点是重复使用的实例还是重新生成的
 * 结论：是同一个实例，根据实例ID对类进行区分
 *
 * @author: chocho
 * @date: 2024/2/21 17:18
 */
//@LiteflowComponent("multiCmp")
public class MultiCmp extends NodeComponent {
    private int a = 0;
    @Override
    public void process() throws Exception {
        System.out.println("===========" + a + "===============");
        a = 2;
    }
}
