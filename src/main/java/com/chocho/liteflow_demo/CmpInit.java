package com.chocho.liteflow_demo;

import com.chocho.liteflow_demo.component.MultiCmp;
import com.yomahub.liteflow.builder.LiteFlowNodeBuilder;
import com.yomahub.liteflow.enums.NodeTypeEnum;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.stereotype.Component;

@Component
public class CmpInit implements SmartInitializingSingleton {
    @Override
    public void afterSingletonsInstantiated() {
        LiteFlowNodeBuilder.createCommonNode().setId("multiCmp1")
                .setType(NodeTypeEnum.COMMON)
                .setName("组件A")
                .setClazz(MultiCmp.class)
                .build();
        LiteFlowNodeBuilder.createCommonNode().setId("multiCmp2")
                .setType(NodeTypeEnum.COMMON)
                .setName("组件B")
                .setClazz(MultiCmp.class)
                .build();
    }
}
