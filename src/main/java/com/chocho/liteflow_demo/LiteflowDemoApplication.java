package com.chocho.liteflow_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiteflowDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiteflowDemoApplication.class, args);
    }

}
