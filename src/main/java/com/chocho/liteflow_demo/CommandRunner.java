package com.chocho.liteflow_demo;

import com.ql.util.express.DefaultContext;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class CommandRunner implements CommandLineRunner {

    @Resource
    private FlowExecutor flowExecutor;
    @Override
    public void run(String... args) throws Exception {
        LiteflowResponse response = flowExecutor.execute2Resp("chain11", "", new DefaultContext<String, Object>());
    }
}
