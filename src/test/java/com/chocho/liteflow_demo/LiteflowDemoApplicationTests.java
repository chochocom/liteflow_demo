package com.chocho.liteflow_demo;


import com.chocho.liteflow_demo.component.MultiCmp;
import com.ql.util.express.DefaultContext;
import com.yomahub.liteflow.builder.LiteFlowNodeBuilder;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.enums.NodeTypeEnum;
import com.yomahub.liteflow.flow.LiteflowResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LiteflowDemoApplicationTests {
    @Autowired
    private ApplicationContext applicationContext;
    @Resource
    private FlowExecutor flowExecutor;

    @Test
    public void contextLoads() {
        LiteFlowNodeBuilder.createCommonNode().setId("multiCmp1")
                .setType(NodeTypeEnum.COMMON)
                .setName("组件A")
                .setClazz(MultiCmp.class)
                .build();
        LiteFlowNodeBuilder.createCommonNode().setId("multiCmp2")
                .setType(NodeTypeEnum.COMMON)
                .setName("组件B")
                .setClazz(MultiCmp.class)
                .build();
        //获取IOC，查看实例是否存在
        MultiCmp multiCmp1 = applicationContext.getBean("multiCmp1", MultiCmp.class);
        MultiCmp multiCmp2 = applicationContext.getBean("multiCmp2", MultiCmp.class);
        LiteflowResponse response = flowExecutor.execute2Resp("chain11", "", new DefaultContext<String, Object>());
    }

}
